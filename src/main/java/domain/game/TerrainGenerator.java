package domain.game;

import domain.difficulty.Level;
import domain.difficulty.OpponentLevel;
import domain.terrain.Coordinate;
import domain.terrain.SimpleTerrain;
import domain.terrain.Terrain;
import domain.terrain.opponent.Juror;
import domain.terrain.opponent.Opponent;
import domain.terrain.tile.BasicTile;
import domain.terrain.tile.SlipperyTile;
import domain.terrain.tile.Tile;

import java.util.HashMap;
import java.util.Optional;
import java.util.Random;
import java.util.stream.IntStream;

public class TerrainGenerator {

    public static Terrain generate(Level level) {
        //generate field of basic tiles. TODO: add options for custom tiles
        HashMap<Coordinate, Tile> tiles = new HashMap<>();
        HashMap<Coordinate, Opponent> opponents = new HashMap<>();
        IntStream.range(0, level.getWidth()).forEachOrdered(
                i -> IntStream.range(0, level.getHeight())
                        .mapToObj(j -> new Coordinate(j, i))
                        .forEachOrdered(c -> {
                            tiles.put(c, generateTile());
                            generateOpponent(level.getOpponentLevel()).ifPresent(opponent -> opponents.put(c, opponent));
                        })
        );
        return new SimpleTerrain(tiles, opponents, level.getWidth(), level.getHeight());
    }

    private static Tile generateTile() {
        //20% chance to get a slippery tile, the rest are basic
        int rnd = new Random().nextInt(4);
        return rnd > 2 ? new SlipperyTile() : new BasicTile();
    }

    private static Optional<Opponent> generateOpponent(OpponentLevel opponentLevel) {
        //10% chance to get an opponent
        int rnd = new Random().nextInt(9);
        return rnd > 7 ? Optional.of(new Juror(opponentLevel)) : Optional.empty();
    }
}
