package domain.player;

import domain.exception.MoveNotAllowed;
import domain.terrain.Coordinate;

public interface Moveable {

    void moveTo(Coordinate location) throws MoveNotAllowed;
}
