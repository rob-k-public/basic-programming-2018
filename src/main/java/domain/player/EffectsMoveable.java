package domain.player;

import domain.exception.MoveNotAllowed;

public interface EffectsMoveable {

    void moveUp() throws MoveNotAllowed;

    void moveLeft() throws MoveNotAllowed;

    void moveRight() throws MoveNotAllowed;

    void moveDown() throws MoveNotAllowed;

}
