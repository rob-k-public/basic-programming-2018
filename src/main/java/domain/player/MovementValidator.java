package domain.player;

import domain.exception.MoveNotAllowed;
import domain.terrain.Coordinate;

class MovementValidator {

    private MovementValidator() {
    }

    static void validate(Coordinate from, Coordinate to) throws MoveNotAllowed {
        if (Math.abs(from.getX() - to.getX()) + Math.abs(from.getY() - to.getY()) > 1) {
            throw new MoveNotAllowed("Only horizontal/vertical moves of one block are allowed.");
        }
    }
}
