package domain.player.skill;

public class Dancing implements Skill {

    private int level;

    public Dancing(int level) {
        this.level = level;
    }

    @Override
    public int getSkillLevel() {
        return level;
    }

    @Override
    public SkillType getType() {
        return SkillType.DANCING;
    }

}
