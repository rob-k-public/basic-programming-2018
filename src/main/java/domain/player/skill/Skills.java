package domain.player.skill;

import java.util.List;
import java.util.Optional;

public class Skills {

    private List<Skill> skills;

    public Skills(List<Skill> skills) {
        this.skills = skills;
    }

    public Optional<Skill> getSkill(SkillType type) {
        return skills.stream()
                .filter(skill -> skill.getType() == type)
                .findFirst();
    }
}
