package domain.player.skill;

public interface Skill {

    int getSkillLevel();

    SkillType getType();

}
