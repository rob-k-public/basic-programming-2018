package domain.player;

import domain.player.skill.Skills;

public interface Interactable {

    Skills getSkills();

    void win();

    void lose();

}
