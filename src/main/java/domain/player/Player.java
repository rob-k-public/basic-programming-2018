package domain.player;

import domain.terrain.Coordinate;
import domain.terrain.PlayerListener;

public class Player implements Moveable {

    private final PlayerListener playerListener;

    private Coordinate location;

    public Player(PlayerListener playerListener) {
        this.playerListener = playerListener;
        location = new Coordinate(0, 0);
        playerListener.playerMoved(location);
    }

    @Override
    public void moveTo(Coordinate coordinate) {
        MovementValidator.validate(location, coordinate);
        location = coordinate;
        playerListener.playerMoved(location);
    }
}
