package domain.difficulty;

public enum Level {

    EASY(4,6,OpponentLevel.EASY),
    HARD(7,9,OpponentLevel.HARD);

    private int width;
    private int height;
    private OpponentLevel opponentLevel;

    Level(int width, int height, OpponentLevel opponentLevel) {
        this.width = width;
        this.height = height;
        this.opponentLevel = opponentLevel;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public OpponentLevel getOpponentLevel() {
        return opponentLevel;
    }
}
