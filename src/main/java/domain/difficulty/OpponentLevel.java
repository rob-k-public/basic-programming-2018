package domain.difficulty;

public enum OpponentLevel {

    EASY(60),
    HARD(70);

    private int mimumJuror;

    OpponentLevel(int mimumJuror) {
        this.mimumJuror = mimumJuror;
    }

    public int getMimumJuror() {
        return mimumJuror;
    }
}
