package domain.logging;

public class ConsoleLogger implements Logger {

    private Class loggingClass;

    public ConsoleLogger(Class loggingClass) {
        this.loggingClass = loggingClass;
    }

    @Override
    public void info(String message) {
        System.out.println(LogMessage.info(message) + " <<< " + loggingClass.toString());
    }

    @Override
    public void warn(String message) {
        System.out.println(LogMessage.warn(message) + " <<< " + loggingClass.toString());
    }
}
