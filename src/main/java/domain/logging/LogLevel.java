package domain.logging;

public enum LogLevel {

    INFO,
    WARN,
    ERROR,

}
