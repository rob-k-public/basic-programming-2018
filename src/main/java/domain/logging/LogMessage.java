package domain.logging;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LogMessage {

    private LogLevel level;
    private Date timestamp;
    private String message;

    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

    private LogMessage(LogLevel level, String message) {
        this.level = level;
        this.message = message;
        timestamp = new Date();
    }

    static LogMessage info(String message) {
        return new LogMessage(LogLevel.INFO, message);
    }

    static LogMessage warn(String message) {
        return new LogMessage(LogLevel.WARN, message);
    }

    @Override
    public String toString() {
        return dateFormat.format(timestamp) + " - " + level.name() + " - " + message;
    }
}
