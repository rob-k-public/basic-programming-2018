package domain.logging;

public interface Logger {

    void info(String message);

    void warn(String message);

}
