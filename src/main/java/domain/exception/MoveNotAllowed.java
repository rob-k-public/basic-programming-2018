package domain.exception;

public class MoveNotAllowed extends IllegalArgumentException {

    public MoveNotAllowed(String s) {
        super(s);
    }
}
