package domain.terrain;

import domain.logging.ConsoleLogger;
import domain.logging.Logger;
import domain.terrain.opponent.Opponent;
import domain.terrain.prize.Prize;
import domain.terrain.tile.Tile;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

public abstract class AbstractTerrain implements Terrain {

    private final Logger logger = new ConsoleLogger(getClass());

    private Map<Coordinate, Tile> tiles;
    private Map<Coordinate, Opponent> opponents;
    private Prize prize;
    private Coordinate playerLocation;
    private int width;
    private int height;

    AbstractTerrain(Map<Coordinate, Tile> tiles, Map<Coordinate, Opponent> opponents, int width, int height) {
        this.tiles = tiles;
        this.opponents = opponents;
        this.width = width;
        this.height = height;
        prize = new Prize(this);
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public Stream<Coordinate> getCoordinates() {
        return tiles.keySet().stream().sorted();
    }

    @Override
    public Tile getTile(Coordinate coordinate) {
        return tiles.get(coordinate);
    }

    @Override
    public Optional<Opponent> getOpponent(Coordinate coordinate) {
        try {
            //map throws nullpointer when no value is present for key . . .
            return Optional.of(opponents.get(coordinate));
        } catch (NullPointerException e) {
            return Optional.empty();
        }
    }

    @Override
    public Prize getPrize() {
        return prize;
    }

    @Override
    public void playerMoved(Coordinate coordinate) {
        playerLocation = coordinate;
        logger.info("Player moved. New location: " + coordinate.toString());
    }

    @Override
    public String toString() {
        return "AbstractTerrain{" +
                "tiles=" + tiles +
                ", opponents=" + opponents +
                ", prize=" + prize +
                ", width=" + width +
                ", height=" + height +
                '}';
    }
}
