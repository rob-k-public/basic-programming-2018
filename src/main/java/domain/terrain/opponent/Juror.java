package domain.terrain.opponent;

import domain.difficulty.OpponentLevel;
import domain.logging.ConsoleLogger;
import domain.logging.Logger;
import domain.player.Interactable;
import domain.player.skill.Skill;
import domain.player.skill.SkillType;

import java.util.Optional;

public class Juror implements Opponent {

    private Logger logger = new ConsoleLogger(getClass());

    private int minimumDancingLevel;

    public Juror(OpponentLevel opponentLevel) {
        minimumDancingLevel = opponentLevel.getMimumJuror();
    }

    @Override
    public void interact(Interactable interactable) {
        Optional<Skill> dancingSkill = interactable.getSkills().getSkill(SkillType.DANCING);
        if (dancingSkill.isPresent()) {
            if (dancingSkill.get().getSkillLevel() >= minimumDancingLevel) {
                interactable.win();
            } else {
                interactable.lose();
            }
        } else {
            logger.warn("Interactable has no dancing skills. Instant lose.");
            interactable.lose();
        }
    }

    @Override
    public String toString() {
        return "Juror";
    }
}
