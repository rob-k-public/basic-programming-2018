package domain.terrain.opponent;

import domain.player.Interactable;

public interface Opponent {

    void interact(Interactable interactable);

}
