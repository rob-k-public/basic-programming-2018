package domain.terrain.prize;

import domain.logging.ConsoleLogger;
import domain.logging.Logger;
import domain.terrain.Coordinate;
import domain.terrain.Terrain;

import java.util.Random;

public class Prize {

    private Logger logger = new ConsoleLogger(getClass());

    private Coordinate location;

    public Prize(Terrain terrain) {
        location = findRandomLocationInField(terrain);
    }

    private Coordinate findRandomLocationInField(Terrain terrain) {
        Random random = new Random();
        Coordinate coordinate = new Coordinate(
                random.nextInt(terrain.getWidth()),
                random.nextInt(terrain.getHeight())
        );
        logger.info("Prize is at " + coordinate.toString());
        return coordinate;
    }

    public Coordinate getLocation() {
        return location;
    }

    @Override
    public String toString() {
        return "Prize";
    }
}
