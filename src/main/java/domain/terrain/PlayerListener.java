package domain.terrain;

public interface PlayerListener {

    void playerMoved(Coordinate coordinate);

}
