package domain.terrain;

import domain.terrain.opponent.Opponent;
import domain.terrain.prize.Prize;
import domain.terrain.tile.Tile;

import java.util.Optional;
import java.util.stream.Stream;

public interface Terrain extends PlayerListener {

    int getWidth();

    int getHeight();

    Stream<Coordinate> getCoordinates();

    Tile getTile(Coordinate coordinate);

    Optional<Opponent> getOpponent(Coordinate coordinate);

    Prize getPrize();

}
