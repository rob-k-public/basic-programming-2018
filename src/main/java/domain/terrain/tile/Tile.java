package domain.terrain.tile;

import domain.player.EffectsMoveable;

public interface Tile {

    void executeEffect(EffectsMoveable moveable);

}
