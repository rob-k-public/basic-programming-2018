package domain.terrain.tile;

import domain.logging.ConsoleLogger;
import domain.logging.Logger;
import domain.player.EffectsMoveable;

import java.util.Random;

public class SlipperyTile implements Tile {

    private Logger logger = new ConsoleLogger(getClass());

    public void executeEffect(EffectsMoveable moveable) {
        moveRandomDirection(moveable);
    }

    private void moveRandomDirection(EffectsMoveable moveable) {
        int rnd = new Random().nextInt(3);
        switch (rnd) {
            case 0:
                moveable.moveUp();
                break;
            case 1:
                moveable.moveLeft();
                break;
            case 2:
                moveable.moveRight();
                break;
            case 3:
                moveable.moveDown();
                break;
            default:
                logger.warn("Random number was out of bounds, not moving. Number was: " + rnd);
                break;
        }
    }

    @Override
    public String toString() {
        return "SlipperyTile";
    }
}

