package domain.terrain.tile;

import domain.player.EffectsMoveable;

public class BasicTile implements Tile {

    public void executeEffect(EffectsMoveable moveable) {
        //no extra effect from landing on basic tile
    }

    @Override
    public String toString() {
        return "BasicTile";
    }
}
