package domain.terrain;

import domain.terrain.opponent.Opponent;
import domain.terrain.tile.Tile;

import java.util.Map;

public class SimpleTerrain extends AbstractTerrain {

    public SimpleTerrain(Map<Coordinate, Tile> tiles, Map<Coordinate, Opponent> opponents,
                         int width, int height) {
        super(tiles, opponents, width, height);
    }
}
