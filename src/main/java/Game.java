import domain.difficulty.Level;
import domain.game.TerrainGenerator;
import domain.player.Player;
import domain.terrain.Terrain;
import ui.GameUI;
import ui.SimpleGameUI;

public class Game {

    public static void main(String[] args) {
        Terrain terrain = TerrainGenerator.generate(Level.EASY);
        Player player = new Player(terrain);
        GameUI ui = new SimpleGameUI(terrain, player);
        System.out.println("STARTING UI");
        ui.launch();
    }
}
