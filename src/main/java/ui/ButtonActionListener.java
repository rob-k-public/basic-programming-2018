package ui;

import domain.logging.ConsoleLogger;
import domain.logging.Logger;
import domain.player.Moveable;
import domain.terrain.Coordinate;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonActionListener implements ActionListener {

    private Logger logger = new ConsoleLogger(getClass());

    private Coordinate coordinate;
    private Moveable player;

    public ButtonActionListener(Coordinate coordinate, Moveable player) {
        this.coordinate = coordinate;
        this.player = player;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        logger.info(coordinate.toString() + " clicked.");
        player.moveTo(coordinate);
    }
}
