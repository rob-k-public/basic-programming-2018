package ui;

import domain.player.Moveable;
import domain.terrain.Coordinate;
import domain.terrain.Terrain;
import domain.terrain.opponent.Opponent;
import domain.terrain.prize.Prize;
import domain.terrain.tile.Tile;

import javax.swing.*;
import java.awt.*;
import java.util.Objects;
import java.util.Optional;

public class SimpleGameUI implements GameUI {

    private static final String LINE_SEPARATOR = "<br />";

    private Terrain terrain;
    private Moveable player;

    JFrame frame;

    public SimpleGameUI(Terrain terrain, Moveable player) {
        this.terrain = terrain;
        this.player = player;
        //initialize GUI
        frame = new JFrame("Dancing Swing UI");
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addButtonGrid(terrain);
    }

    private void addButtonGrid(Terrain terrain) {
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(terrain.getHeight(), terrain.getWidth()));
        terrain.getCoordinates().forEach(coordinate -> {
            JButton button = coordinateButton(
                    coordinate,
                    terrain.getTile(coordinate),
                    terrain.getOpponent(coordinate),
                    terrain.getPrize()
            );
            buttonPanel.add(button, coordinate.getX(), coordinate.getY());
        });
        frame.getContentPane().add(buttonPanel);
    }

    private JButton coordinateButton(Coordinate coordinate, Tile tile, Optional<Opponent> opponent, Prize prize) {
        JButton button = new JButton(
                "<html>"
                        + tile.toString()
                        + LINE_SEPARATOR
                        + opponent.map(Opponent::toString).orElse("-")
                        + LINE_SEPARATOR
                        + Optional.of(prize).filter(p -> Objects.equals(p.getLocation(), coordinate)).map(Prize::toString).orElse("-")
                        + "</html>"
        );
        button.addActionListener(new ButtonActionListener(coordinate, player));
        return button;
    }

    @Override
    public void launch() {
        frame.pack();
        frame.setVisible(true);
    }
}
